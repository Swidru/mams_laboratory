package com.mams.laboratory.service;

import android.graphics.Bitmap;

import com.mams.laboratory.model.ImageTagsModel;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import java.util.Map;

public class LabelingService {

    private final float treshold = 0.25f;

    private ImageTagsModel imageTagsModel;
    private DisplayService displayService;
    private CustomTfClassificationService customTfClassificationService;
    private final Map<Integer, String> labels;

    public LabelingService(ImageTagsModel imageTagsModel, DisplayService displayService, TensorFlowInferenceInterface tf, Map<Integer, String> labels) {
        this.imageTagsModel = imageTagsModel;
        this.displayService = displayService;
        customTfClassificationService = new CustomTfClassificationService(tf);
        this.labels = labels;
    }

    public void labelAndDisplayImage(Bitmap image) {
        displayService.setImageOnView(image);
        imageTagsModel.setImage(image);
        float[] predictions = customTfClassificationService.predict(image);
        String tags = getTags(predictions);
        imageTagsModel.setTags(tags);
        displayService.setTextAndImage(imageTagsModel);
    }

    private String getTags(float[] predictions) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < predictions.length; i++) {
            float prediction = predictions[i];
            if (prediction > treshold) {
                String percent = String.format("%.2f", prediction * 100.0f);
                sb.append(labels.get(i)).append(" (" + percent + "%)\n");
            }
        }
        String tags = sb.toString();
        return tags.isEmpty() ? "No fruits were found on the picture with probability over 25%" : tags;
    }
}
