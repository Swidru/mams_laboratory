package com.mams.laboratory.utils;

import android.graphics.Bitmap;

public class ImageUtils {

    public static Bitmap resizeImage(Bitmap bitmap, int size) {
        return Bitmap.createScaledBitmap(bitmap, size, size, false);
    }

    public static float[] normalizeBitmap(Bitmap source, int[] size, float mean, float std) {

        float[] output = new float[size[0] * size[1] * size[2]];
        int[] intValues = new int[source.getHeight() * source.getWidth()];

        source.getPixels(intValues, 0, source.getWidth(), 0, 0, source.getWidth(), source.getHeight());
        for (int i = 0; i < intValues.length; ++i) {
            final int val = intValues[i];
            output[i * 3] = (((val >> 16) & 0xFF) - mean) / std;
            output[i * 3 + 1] = (((val >> 8) & 0xFF) - mean) / std;
            output[i * 3 + 2] = ((val & 0xFF) - mean) / std;
        }

        float min = getMin(output);
        float max = getMax(output);

        for (int i = 0; i < output.length; i++) {
            output[i] = (output[i] - min) / (max - min);
        }

        return output;
    }

    private static float getMax(float[] inputArray) {
        float maxValue = inputArray[0];
        for (int i = 1; i < inputArray.length; i++) {
            if (inputArray[i] > maxValue) {
                maxValue = inputArray[i];
            }
        }
        return maxValue;
    }

    private static float getMin(float[] inputArray) {
        float minValue = inputArray[0];
        for (int i = 1; i < inputArray.length; i++) {
            if (inputArray[i] < minValue) {
                minValue = inputArray[i];
            }
        }
        return minValue;
    }
}
