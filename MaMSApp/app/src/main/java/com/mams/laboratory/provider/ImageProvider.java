package com.mams.laboratory.provider;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.ImageView;

import androidx.core.content.FileProvider;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ImageProvider {

    private final int pickImageCode = 100;
    private final int takeImageCode = 200;

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss");

    private String currentImagePath;
    private final WeakReference<Activity> activity;

    public ImageProvider(Context context) {
        activity = new WeakReference<>((Activity) context);
    }

    public void pickImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        activity.get().startActivityForResult(intent, pickImageCode);
    }

    public void takeImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            File photoFile = createImageFile();
            String authority = "com.mams.laboratory.fileprovider";
            Uri photoURI = FileProvider.getUriForFile(activity.get(), authority, photoFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        } catch (IOException e) {
            e.printStackTrace();
        }
        activity.get().startActivityForResult(intent, takeImageCode);
    }

    public Bitmap getImageFromIntent(int code, Intent data, ImageView imageView) {
        Bitmap image = null;
        switch (code) {
            case pickImageCode:
                Uri selectedImage = data.getData();
                if (selectedImage != null) {
                    image = getImageFromStorage(selectedImage);
                }
                break;
            case takeImageCode:
                int targetWidth = imageView.getWidth();
                int targetHeight = imageView.getHeight();
                image = decodeAndScaleImage(targetWidth, targetHeight);
                break;
        }
        return image;
    }

    private File createImageFile() throws IOException {
        String timeStamp = LocalDateTime.now().format(formatter);
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = activity.get().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        currentImagePath = image.getAbsolutePath();
        return image;
    }

    private Bitmap getImageFromStorage(Uri uri) {
        try {
            Bitmap image = MediaStore.Images.Media.getBitmap(activity.get().getContentResolver(), uri);
            return image;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Bitmap decodeAndScaleImage(int targetWidth, int targetHeight) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(currentImagePath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        int scaleFactor = Math.min(photoW / targetWidth, photoH / targetHeight);

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;

        Bitmap image = BitmapFactory.decodeFile(currentImagePath, bmOptions);
        return image;
    }
}
