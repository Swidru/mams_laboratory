package com.mams.laboratory;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.mams.laboratory.model.ImageTagsModel;
import com.mams.laboratory.provider.ImageProvider;
import com.mams.laboratory.service.DisplayService;
import com.mams.laboratory.service.LabelingService;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    static {
        System.loadLibrary("tensorflow_inference");
    }

    private final String MODEL_PATH = "file:///android_asset/fruits_model.pb";

    private ImageTagsModel imageTagsModel;
    private ImageProvider imageProvider;
    private DisplayService displayService;
    private LabelingService labelingService;
    private TensorFlowInferenceInterface tf;

    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        tf = new TensorFlowInferenceInterface(getAssets(), MODEL_PATH);

        TextView textView = findViewById(R.id.tags_text_view);
        imageView = findViewById(R.id.image_view);

        if (imageTagsModel == null) {
            imageTagsModel = ViewModelProviders.of(this).get(ImageTagsModel.class);
        }

        displayService = new DisplayService(textView, imageView);
        imageProvider = new ImageProvider(this);
        labelingService = new LabelingService(imageTagsModel, displayService, tf, getLabelsFromAssets());

        displayService.setTextAndImage(imageTagsModel);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
            disableTakeImageMenuItem();
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.pick_image_mi:
                imageProvider.pickImage();
                return true;
            case R.id.take_image_mi:
                imageProvider.takeImage();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            displayService.clearView();
            Bitmap image = imageProvider.getImageFromIntent(requestCode, data, imageView);
            if (image != null) {
                processImage(image);
            }
        } else {
            displayService.clearView();
        }
    }

    private void processImage(Bitmap image) {
        displayService.setTextOnView(R.string.processing_info);
        labelingService.labelAndDisplayImage(image);
    }

    private void disableTakeImageMenuItem() {
        View takePictureMenuItem = findViewById(R.id.take_image_mi);
        takePictureMenuItem.setEnabled(false);
    }

    private Map<Integer, String> getLabelsFromAssets() {
        BufferedReader reader = null;
        Map<Integer, String> labels = new HashMap<>();
        try {
            reader = new BufferedReader(new InputStreamReader(getAssets().open("labels.txt")));
            String line;
            int index = 0;
            while ((line = reader.readLine()) != null) {
                labels.put(index, line.trim());
                ++index;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return labels;
        }
    }
}
