package com.mams.laboratory.service;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.mams.laboratory.R;
import com.mams.laboratory.model.ImageTagsModel;

public class DisplayService {

    private TextView textView;
    private ImageView imageView;

    public DisplayService(TextView textView, ImageView imageView) {
        this.textView = textView;
        this.imageView = imageView;
    }

    public void setTextAndImage(ImageTagsModel model) {
        if (model.getImage() != null && !TextUtils.isEmpty(model.getTags())) {
            imageView.setImageBitmap(model.getImage());
            textView.setText(model.getTags());
        }
    }

    public void setTextOnView(int message) {
        textView.setText(message);
    }

    public void setImageOnView(Bitmap image) {
        imageView.setImageBitmap(image);
    }

    public void clearView() {
        imageView.setImageBitmap(null);
        textView.setText(R.string.init_text_view_info);
    }
}
