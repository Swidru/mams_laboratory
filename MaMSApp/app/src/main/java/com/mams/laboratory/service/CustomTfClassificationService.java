package com.mams.laboratory.service;

import android.graphics.Bitmap;

import com.mams.laboratory.utils.ImageUtils;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

public class CustomTfClassificationService {

    private final String tensorInputName = "input_input";
    private final String tensorOutputName = "output_1";
    private final int[] inputSize = {100, 100, 3};
    private final float mean = 127.5f;
    private final float std = 1.0f;
    private final int outputSize = 4;

    private final TensorFlowInferenceInterface tf;


    public CustomTfClassificationService(TensorFlowInferenceInterface tf) {
        this.tf = tf;
    }

    public float[] predict(final Bitmap bitmap) {

        Bitmap resized_image = ImageUtils.resizeImage(bitmap, inputSize[0]);
        float[] floatValues = ImageUtils.normalizeBitmap(resized_image, inputSize, mean, std);

        tf.feed(tensorInputName, floatValues, 1, inputSize[0], inputSize[1], inputSize[2]);
        tf.run(new String[]{tensorOutputName});

        float[] prediction = new float[outputSize];
        tf.fetch(tensorOutputName, prediction);

        return prediction;
    }
}
