package com.mams.laboratory.model;

import android.graphics.Bitmap;

import androidx.lifecycle.ViewModel;

public class ImageTagsModel extends ViewModel {

    private String tags;
    private Bitmap image;

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
}
