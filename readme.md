Original dataset can be found [here](https://zenodo.org/record/1310165#.XOmnClfVKUk "here").

##New model creation

- .zip file needs to be unziped in **../PythonScriptsML/fruits** directory,
- to train model **model_script.py** script needs to be run --> it will save .pb, .h5 and .tflite models in the same folder as script.

To use new model, .pb file in android project assets folder should be replaced with new one.