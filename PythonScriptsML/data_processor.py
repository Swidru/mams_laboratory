import numpy as np
import os
from scipy import misc
import random


class DataProcessor:
    def __init__(self, info_dir):
        self._dir_path = os.path.dirname(os.path.realpath(__file__))
        self._labels, self._image_per_labels_count = self._get_data_from_file(info_dir)

    def get_data(self, directory, check_input_n, n_images_per_class=2000):
        images_paths = self._get_paths(directory)
        random.shuffle(images_paths)
        random.shuffle(images_paths)
        images = []
        labels = []
        for image_path in images_paths:
            label = self._get_label(image_path)
            if label == -1:
                raise Exception("Correct label was not found!")
            if check_input_n and not self._check_if_can_add_image(label, n_images_per_class):
                continue
            else:
                image = misc.imread(image_path)
                image = misc.imresize(image, (100, 100))
                image = image / 255.0
                images.append(image)
                labels.append(label)
        images = np.asarray(images)
        labels = np.asarray(labels)

        return images, labels

    def _check_if_can_add_image(self, label, n_images_per_class):
        count = self._image_per_labels_count.get(label)
        if count >= n_images_per_class:
            return False
        else:
            self._image_per_labels_count[label] = count + 1
            return True

    def _get_paths(self, directory):
        image_paths = []
        path = self._dir_path + directory
        for subdir, dirs, files in os.walk(path):
            for file in files:
                image_paths.append(os.path.join(subdir, file))
        return image_paths

    def _get_data_from_file(self, label_dir):
        labels_dic = {}
        image_per_labels_count = {}
        file = open(self._dir_path + label_dir, "r")
        labels = map(str.strip, file.readlines())
        i = 0
        for label in labels:
            labels_dic[label] = i
            image_per_labels_count[i] = 0
            i += 1
        return labels_dic, image_per_labels_count

    def _get_label(self, image_path):
        for key in self._labels.keys():
            if key in image_path:
                return self._labels[key]
        return -1
