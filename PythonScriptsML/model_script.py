import os
import tensorflow as tf
from keras import backend as K
from tensorflow.contrib import lite
from keras.models import Sequential
from keras.callbacks import ModelCheckpoint
from keras.layers.core import Dense, Dropout, Flatten
from keras.layers.convolutional import Conv2D, MaxPooling2D
import random
from data_processor import DataProcessor
from sklearn.model_selection import train_test_split


def keras_to_tensorflow(keras_model, output_dir, model_name, out_prefix="output_", log_tensorboard=True):
    if os.path.exists(output_dir) == False:
        os.mkdir(output_dir)

    out_nodes = []

    for i in range(len(keras_model.outputs)):
        out_nodes.append(out_prefix + str(i + 1))
        tf.identity(keras_model.output[i], out_prefix + str(i + 1))

    sess = K.get_session()

    from tensorflow.python.framework import graph_util, graph_io

    init_graph = sess.graph.as_graph_def()

    main_graph = graph_util.convert_variables_to_constants(sess, init_graph, out_nodes)

    graph_io.write_graph(main_graph, output_dir, name=model_name, as_text=False)

    if log_tensorboard:
        from tensorflow.python.tools import import_pb_to_tensorboard

        import_pb_to_tensorboard.import_to_tensorboard(
            os.path.join(output_dir, model_name),
            output_dir)


def create_model(img_shape=(100, 100, 3), n_classes=3):
    model = Sequential()
    model.add(Conv2D(32, kernel_size=(3, 3),
                     activation='relu',
                     input_shape=img_shape, name='input'))
    # model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(50, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(n_classes, activation='softmax', name='output'))
    return model


seed = random.randint(1, 2 ** 32 - 1)
data_dir = "\\fruits\\images\\"
labels_dir = "\\fruits\\labels.txt"

data_processor = DataProcessor(labels_dir)

images, labels = data_processor.get_data(data_dir, True)
n_outputs = max(labels) + 1

image_w, image_h, channels = images[0].shape

train_images, test_images, train_labels, test_labels = train_test_split(images, labels, test_size=0.2,
                                                                        random_state=seed)

model = create_model(n_classes=n_outputs)

model.summary()

model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

check_pointer_path = os.path.dirname(os.path.realpath(__file__)) + "\\checkpoint\\weights_fruits_model.hdf5"
check_pointer = ModelCheckpoint(filepath=check_pointer_path, verbose=1, save_best_only=True)

model.fit(train_images, train_labels, epochs=15, validation_data=(test_images, test_labels), batch_size=15,
          callbacks=[check_pointer])

model_file = "fruits_model_big.h5"
model.save(model_file)

eval_images, eval_labels = data_processor.get_data("\\fruits\\test\\", False)

print(eval_labels)
print(model.predict(eval_images))

converter = lite.TFLiteConverter.from_keras_model_file(model_file)
tflitle_model = converter.convert()
open("fruits_model.tflite", "wb").write(tflitle_model)

interpreter = lite.Interpreter(model_content=tflitle_model)
interpreter.allocate_tensors()

path = os.path.dirname(os.path.realpath(__file__))
keras_to_tensorflow(model, path, "fruits_model.pb")
